package com.sanitas.factory;

import com.sanitas.factory.ifaces.IOperation;
import com.sanitas.factory.impl.SumOperation;
import com.sanitas.factory.impl.SustrandOperation;
import com.sanitas.model.CalculatorInputRequest;
import com.sanitas.model.OperationType;
import org.springframework.stereotype.Component;

@Component
public class OperationFactory {

  public IOperation getOperation(CalculatorInputRequest inputRequest){

    if(inputRequest.getType() == OperationType.SUM){
      return new SumOperation();

    } else return new SustrandOperation();
  }
}
