package com.sanitas.factory.impl;

import com.sanitas.factory.ifaces.IOperation;
import com.sanitas.model.CalculatorInputRequest;
import org.springframework.stereotype.Service;

@Service
public class SumOperation implements IOperation {

  @Override
  public double execute(CalculatorInputRequest inputRequest) {

    double result = inputRequest.getOperator1()+inputRequest.getOperator2();
    trace.trace(result);
    return result;
  }
}
