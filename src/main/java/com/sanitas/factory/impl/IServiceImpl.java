package com.sanitas.factory.impl;

import com.sanitas.factory.OperationFactory;
import com.sanitas.factory.ifaces.IService;
import com.sanitas.model.CalculatorInputRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class IServiceImpl implements IService {

  private OperationFactory operationFactory;

  @Autowired
  public IServiceImpl(OperationFactory operationFactory) {
    this.operationFactory = operationFactory;
  }

  @Override
  public double executeOperation(CalculatorInputRequest inputRequest) {
    return operationFactory.getOperation(inputRequest).execute(inputRequest);
  }
}
