package com.sanitas.factory.impl;

import com.sanitas.factory.ifaces.IOperation;
import com.sanitas.model.CalculatorInputRequest;

public class SustrandOperation implements IOperation {

  @Override
  public double execute(CalculatorInputRequest inputRequest) {

    double result = inputRequest.getOperator1() - inputRequest.getOperator2();
    trace.trace(result);
    return result;
  }
}
