package com.sanitas.factory.ifaces;

import com.sanitas.model.CalculatorInputRequest;

public interface IService {

  double executeOperation(CalculatorInputRequest inputRequest);
}
