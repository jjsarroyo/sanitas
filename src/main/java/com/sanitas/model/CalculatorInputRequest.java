package com.sanitas.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CalculatorInputRequest {

  private double operator1;
  private double operator2;
  private OperationType type;
}
