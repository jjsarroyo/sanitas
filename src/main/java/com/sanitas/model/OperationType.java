package com.sanitas.model;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum OperationType {

  SUM("sum"), SUBSTRACT("substract");

  private String value;

  @JsonValue
  public String getValue() {
    return value;
  }
}
