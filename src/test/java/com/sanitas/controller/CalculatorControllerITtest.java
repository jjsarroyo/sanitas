package com.sanitas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanitas.model.CalculatorInputRequest;
import com.sanitas.model.OperationType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CalculatorControllerITtest {

  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void whenValidInputSum_thenReturns200() throws Exception {

    CalculatorInputRequest input = new CalculatorInputRequest(2d, 2d, OperationType.SUM);

    mockMvc.perform(MockMvcRequestBuilders.post("/calculator/v1/execute")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(input)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.result").value(4.0));
  }

  @Test
  public void whenValidInputSubstract_thenReturns200() throws Exception {

    CalculatorInputRequest input = new CalculatorInputRequest(4d, 2d, OperationType.SUBSTRACT);

    mockMvc.perform(MockMvcRequestBuilders.post("/calculator/v1/execute")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(input)))
        .andExpect(MockMvcResultMatchers.jsonPath("$.result").value(2.0));

  }
}