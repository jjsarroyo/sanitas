package com.sanitas.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sanitas.factory.ifaces.IService;
import com.sanitas.model.CalculatorInputRequest;
import com.sanitas.model.OperationType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CalculatorController.class)
public class CalculatorControllerTest {

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private IService service;

  @Autowired
  private ObjectMapper objectMapper;

  @Test
  public void whenValidInputSum_thenReturns200() throws Exception {

    CalculatorInputRequest input = new CalculatorInputRequest(2d,2d, OperationType.SUM);

    mockMvc.perform(MockMvcRequestBuilders.post("/calculator/v1/execute")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(input)))
        .andExpect(status().isOk());
  }

  @Test
  public void whenValidInputSubstract_thenReturns200() throws Exception {

    CalculatorInputRequest input = new CalculatorInputRequest(2d,2d, OperationType.SUBSTRACT);

    mockMvc.perform(MockMvcRequestBuilders.post("/calculator/v1/execute")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(input)))
        .andExpect(status().isOk());
  }
}